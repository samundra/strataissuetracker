from flask import Flask, render_template, g, request, redirect, url_for,session
from flaskext.mysql import MySQL
from werkzeug.security import generate_password_hash, check_password_hash
import os

app = Flask(__name__)

app.config['DEBUG'] = True
app.config['SECRET_KEY'] = os.urandom(24)

# MYSQL configurations
app.config['MYSQL_DATABASE_USER'] = 'strata_admin'
app.config['MYSQL_DATABASE_PASSWORD'] = 'Strata@123'
app.config['MYSQL_DATABASE_DB'] = 'strata'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'

flaskmysql = MySQL()
flaskmysql.init_app(app)


def connect_db(flaskmysql):
    """
    connecting to the database
    :param flaskmysql:
    :return:
    """
    con = flaskmysql.connect()
    return con


def get_db(flaskmysql):
    if not hasattr(g,'mysql_db'):
        g.mysql_db = connect_db(flaskmysql)
    return g.mysql_db


@app.teardown_appcontext
def close_db(error):
    """
    dissconnecting the database
    :param error:
    :return:
    """
    if hasattr(g, 'mysql_db'):
        g.mysql_db.close()


def get_current_user():
    if 'user' in session:
        db = get_db(flaskmysql)
        cur = db.cursor()
        cur.execute("select * from users where email = %s", [session['user']])
        result = cur.fetchone()
        return result
    return None


@app.route('/')
def index():
    """
    index page with list of the issues
    :return:
    """
    # verifing session
    user_result = get_current_user()
    if user_result is None:
        print('none')
        return redirect(url_for('login'))
    # Getting data from database
    db = get_db(flaskmysql)
    cur = db.cursor()
    cur.execute("Select * from issues")
    results = cur.fetchall()
    dis_result = {}
    dis_result = dict(results)
    print(dis_result)
    return render_template('index.html', result = dis_result, user=user_result[1])


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        db = get_db(flaskmysql)
        cur = db.cursor()

        hashedpass = generate_password_hash(request.form['pass'],method="sha256")
        try:
            cur.execute("insert into users (email,password_hash,admin) values(%s,%s,%s)", [request.form['email'], hashedpass, '0'])
            db.commit()
        except:
            return render_template('signup.html', error="Email Already Exists")

        return render_template('login.html', error="User created")
    return render_template('signup.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        db = get_db(flaskmysql)
        cur = db.cursor()
        cur.execute("select * from users where email = %s",[request.form['email']])
        result = cur.fetchone()

        # checking the login detail
        if result and check_password_hash(result[2], request.form['pass']):
            session['user'] = result[1]
            session['admin'] = result[3]
            return redirect(url_for('index'))
        else:
            error = "Login information Not Correct"

    return render_template('login.html',error = error)


@app.route('/logout')
def logout():
    session.pop('user')
    session.pop('admin')
    return redirect(url_for('index'))

@app.route('/new', methods=['GET','POST'])
def new_issue():
    """
    Create a new issue
    :return:
    """
    # verifing session
    user_result = get_current_user()
    if user_result is None:
        print('none')
        return redirect(url_for('login'))

    #connecting to db
    db = get_db(flaskmysql)
    cur = db.cursor()
    if request.method == 'GET':
        return render_template('new_issue.html',user=user_result[1])

    des = request.form['description']

    cur.execute("insert into issues (issue_description) values (%s) ", (des))
    db.commit()

    return redirect(url_for('index'))


@app.route('/edit/<id>',methods = ['POST','GET'])
def edit(id):
    """
    edit the issue
    :param id:
    :return: edit or index page
    """
    # verifing session
    user_result = get_current_user()
    if user_result is None:
        print('none')
        return redirect(url_for('login'))

    db = get_db(flaskmysql)
    cur = db.cursor()

    if request.method == 'GET':
        cur.execute("select * from issues where issue_number = %s ", [id])
        result = cur.fetchone()
        print(user_result[3])
        return render_template('edit.html',result = result,user=user_result[1], admin=user_result[3])

    else:
        des = request.form['description']
        cur.execute("update issues set issue_description = %s where issue_number = %s",[des,id])
        db.commit()
        return redirect(url_for('index'))


@app.route('/deleteissue/<id>')
def delete_issue(id):
    """
    deleting issue
    :param id:
    :return:
    """
    # verifing session
    user_result = get_current_user()
    if user_result is None:
        print('none')
        return redirect(url_for('login'))

    db = get_db(flaskmysql)
    cur = db.cursor()
    cur.execute('delete from issues where issue_number = %s',[id])
    db.commit()
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run()